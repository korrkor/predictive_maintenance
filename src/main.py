import sys
from  utility.Preprocessing.DataCleaning import * 
from  utility.Preprocessing.DataExploration import * 
from  utility.FailurePrediction.AnomalyDetectionBaseline import *
#from  utility.FailurePrediction.MahalanobisBinaryClassifier import *
#from  utility.FailurePrediction.FailurePredictionLogistic import *
from  utility.Utils.Util import *
from  utility.Utils.S3_Connection import *


def main():
  df = read_csv(data_path+filename)
  # df = get_S3_file(filename)
  logger = Logger("Data Preprocessing - DataCleaning")
   
  logger.info("DATA CLEANING *****************************************************")
  dc = DataCleaning()
  logger.info("Populating Manufacturers *****************************")
  df_temp = dc.populate_MFG(df)
  logger.info("Converting to datetime *****************************")
  df_temp = dc.convert_to_datetime(df_temp)
  # logger.info("Dropping columns with more than 90% nan *****************************")
   # df_temp = dc.drop_nan(df_temp) 
  logger.info("Filling null values **************************")
  df_temp = dc.fill_missing_values(df_temp)
  logger.info("Removing out of bounds temperatures *****************************")
  df_temp = dc.out_of_bounds_temperature(df_temp)
  logger.info("Removing out of bounds drive days *****************************")
  df_temp = dc.out_of_bounds_drive_days(df_temp)


  logger.info("Changing cols to float *****************************")
  df_clean = dc.change_cols_to_float(df_temp)

  logger.info("Removing normalised features *****************************")
  df_temp = dc.remove_normalised_features(df_clean)
  logger = Logger("Data Preprocessing - DataExploration")
  

  
  logger.info("DATA EXPLORATION **************************************************************")
  de = DataExploration()
  logger.info("Total number of drives ************************************")
  de.get_num_drives(df_temp)
  logger.info("Total number of alive drives ************************************")
  de.get_num_alive(df_temp) 
  logger.info("Total number of failed drives ************************************")
  de.get_num_failed(df_temp) 
  logger.info("Caculating corr between temp and failure ************************************")
  de.calculate_temperature_average(df_temp)
  
  logger = Logger("Failure Prediction - Baseline")
  logger.info("FAILURE PREDICTION **************************************************************")
  fpb = AnomalyDetectionBaseline()
  df_temp = fpb.get_features(df_temp)
  df_fpb = fpb.create_predicted_failure(df_temp)
  fpb.baseline_performance(df_fpb)



#     logger = Logger("Failure Prediction - Mahalanobis Distance")

#     logger.info("Starting logistic Regression ******************************")

#     df_mbc = remove_raw_features(df_clean)

#     clf = MahalanobisBinaryClassifier(df_mbc)   

#     clf.classifier() 



# #     logger = Logger("Predictive Maintenance - FailurePrediction(LogisticRegression)")

# #     logger.info("Starting logistic Regression ******************************")

# #     fpl = FailurePredictionLogistic()

# #     logger.info("Splitting the data into test and train : 0.25 test ************************** ")

#     # X_train,X_test,y_train,y_test= split_data(df_fpb)

# #     logger.info("Training the logistic regression model and predicting failure***************")

# #     y_pred, cnf_matrix= fpl.logistic_regression(X_train,X_test,y_train,y_test)

# #     logger.info("Create heatmap of confusion matrix ********************** ")

# #     fpl.create_heat_map(y_pred,y_test,cnf_matrix)

# #     logger.info("Creating ROC curve of the logistic regression model  *******************")

# #     fpl.roc_curve(X_test,y_test)



if __name__ == '__main__':

	main()















#    # df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/BreastCancer.csv', 

#     #              usecols=['Cl.thickness', 'Cell.size', 'Marg.adhesion', 

#     #                       'Epith.c.size', 'Bare.nuclei', 'Bl.cromatin', 'Normal.nucleoli', 

#     #                       'Mitoses', 'Class'])



#     # df.dropna(inplace=True)  # drop missing values.



#     clf = MahalanobisBinaryClassifier(df)   

#     clf.classifier()
