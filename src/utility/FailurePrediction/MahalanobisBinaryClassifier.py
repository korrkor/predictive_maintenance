from utility.utils import *
class MahalanobisBinaryClassifier():
    def __init__(self, df):
        self.predictors = ["smart_5_normalized","smart_187_normalized","smart_188_normalized","smart_197_normalized","smart_198_normalized"]
        self.xtrain, self.xtest, self.ytrain, self.ytest = self.split_data(df)
        self.xtrain_pos = self.xtrain.loc[self.ytrain == 1, :]
        self.xtrain_neg = self.xtrain.loc[self.ytrain == 0, :]    
        #print(self.xtrain_pos)
        
    def get_features(self,df):
        features = ["failure"] +  self.predictors
        return cache(get_select_columns(df, features))

    def split_data(self,df):
        df.select('failure').distinct().show()
        new_df = self.get_features(df)
        new_df = new_df.toPandas()
        new_df.dropna(inplace=True)
        new_df.replace(0.0, 
           new_df[self.predictors].mean(), 
           inplace=True)
        X = new_df[self.predictors]
        y = new_df.failure
        return train_test_split(X,y,test_size=0.25,random_state=0)

    def predict_proba(self, xtest):
        pos_neg_dists = [(p,n) for p, n in zip(self.mahalanobis(xtest, self.xtrain_pos), self.mahalanobis(self.xtest, self.xtrain_neg))]
        return np.array([(1-n/(p+n), 1-p/(p+n)) for p,n in pos_neg_dists])

    def predict(self, xtest):
        return np.array([np.argmax(row) for row in self.predict_proba(self.xtest)])

    def mahalanobis(self, x=None, data=None, cov=None):
        """Compute the Mahalanobis Distance between each row of x and the data  
        x    : vector or matrix of data with, say, p columns.
        data : ndarray of the distribution from which Mahalanobis distance of each observation of x is to be computed.
        cov  : covariance matrix (p x p) of the distribution. If None, will be computed from data.
        """

        x_minus_mu = x - np.mean(data)
        print("this is the darta a a a a a a aa  a")
        print(data)
        if cov == None:
            cov = np.cov(data.values.T)
            print("here 2 **************************")
            print(cov)
        inv_covmat = sp.linalg.pinv(cov)
        left_term = np.dot(x_minus_mu, inv_covmat)
        mahal = np.dot(left_term, x_minus_mu.T)
        return mahal.diagonal()
    
    def classifier(self):
        pred_probs = self.predict_proba(self.xtest)
        pred_class = self.predict(self.xtest)
        pred_actuals = pd.DataFrame([(pred, act) for pred, act in zip(pred_class, self.ytest)], columns=['pred', 'true'])
        print(pred_actuals[:5])        
        truth = pred_actuals.loc[:, 'true']
        pred = pred_actuals.loc[:, 'pred']
        scores = np.array(pred_probs)[:, 1]
        print("AUROC: ", roc_auc_score(truth, scores))
        print("Confusion Matrix: \n")
        print(confusion_matrix(truth, pred))
        print("Accuracy Score: ")
        print(accuracy_score(truth, pred))
        print("Classification Report: \n" )
        print(classification_report(truth, pred))
