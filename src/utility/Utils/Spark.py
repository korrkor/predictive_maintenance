from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
from pyspark.sql.functions import *
from pyspark import SparkContext
from os.path import expanduser, join, abspath
#warehouse_location = abspath('spark-warehouse')
def get_spark_session(nome):
	spark = (SparkSession.builder
		.appName(nome)
        .getOrCreate()
	)   
	return spark
