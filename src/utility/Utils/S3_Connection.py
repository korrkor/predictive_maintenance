from utility.Utils.Util import *
import pandas as pd
from utility.Utils.Spark import *
spark = get_spark_session("")
spark.sparkContext.setLogLevel("FATAL")


session = boto3.Session(
                    aws_access_key_id=aws_access_key_id,
                    aws_secret_access_key=aws_secret_access_key,
                    region_name=region)

s3_resource = session.resource('s3')
def S3_load_hive():
    obj = s3_resource.Object(bucket,data_path+"2018-11-23.csv") 
    df = pd.read_csv(obj.get()['Body'])
    #print(df.info())
    data = convert_pandas_to_spark(df)
    data = data.withColumn("partition_date",F.date_format('date','yyyy-MM') )
    create_or_insert_table("default", "hard", data, partitions=['partition_date'])
    #data.show()

def get_S3_file(filename):
    session = boto3.Session(
                    aws_access_key_id=aws_access_key_id,
                    aws_secret_access_key=aws_secret_access_key,
                    region_name=region)
    s3_resource = session.resource('s3')
    obj = s3_resource.Object(bucket,data_path+filename) 
    df = pd.read_csv(obj.get()['Body'])
    data = convert_pandas_to_spark(df)
    return data

# def get_S3_file(filename):
#     obj = s3_resource.Object(bucket,data_path+filename)
#     df = pd.read_csv(obj.get()['Body'])
#     #print(df.info())
#     data = convert_pandas_to_spark(df)
#     return data